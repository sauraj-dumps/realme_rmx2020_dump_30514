## fuse_RMX2020-eng 12 SP1A.210812.016 eng.jaishn.20211007.130322 test-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2020
- Brand: realme
- Flavor: fuse_RMX2020-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.jaishn.20211007.130322
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/fuse_RMX2020/RMX2020:12/SP1A.210812.016/jaishnav10071440:eng/test-keys
- OTA version: 
- Branch: fuse_RMX2020-eng-12-SP1A.210812.016-eng.jaishn.20211007.130322-test-keys
- Repo: realme_rmx2020_dump_30514


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
